clear all
clc
close all

s_folder='signals/';
s_file='p1_rec_1m.mat';
% s_file='p4_rec_1m.mat';
% s_file='p7_rec_1m.mat';
% s_file='p8_rec_1m.mat';
% s_file='p10_rec_1m.mat'
% s_file='p14_rec_1m.mat';
% s_file='p16_rec_1m.mat';
% s_file='p19_rec_1m.mat';
% s_file='p21_rec_1m.mat';
% s_file='p24_rec_1m.mat';
filename=strcat(s_folder, s_file);
file = load(filename);
values = file.val - mean(file.val);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% N�sleduj�c� parametry je mo�n� upravovat a testovat takfunk?nost %
% algoritmu                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N_samp = 500;               % Vzorkovac� frekvence
HP_filter_length = 5;       % D�lka high-pass filtru
LP_filter_window = 12;      % D�lka okna low-pass filtru
Threshold_window = 325;     % D�lka okna hled�n� prahu
Delta_threshold = 60;       % D�lka okna korekce hodnot prahov�n�

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

time = 60 / (size(values,2) / N_samp); % vypo?�t� dobu m??en� sign�lu

figure('Name', 'Detekce QRS komplexu - Chenuv algoritmus');

subplot(4,1,1);
plot(values);
title('Vstupni signal');
ylabel('Napeti [mV]');
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Line�rn� High-Pass Filtr         %
%                                         %
%   y1[n] = 1/M * sum_0^(M-1) (x[n - m])  %
%   y2[n] = x [n - ((M + 1) / 2)]         %
%   y[n] = y2[n] - y1[n]                  %
%                                         %
%   M - d�lka filtru                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y1 = zeros(1, size(values,2));               % inicializace y1
y2 = zeros(1, size(values,2));               % inicializace y2
M = HP_filter_length;                        % inicializace d�lky filtru

for n = 1 : size(values, 2)
    sum = 0;
    for m = 1 : (M - 1)
        if (n - m) >= 1
            sum = sum + values(n - m);
        end
    end
    y1(n) = (1 / M) * sum;
    
    y2_index = cast(n - ((M + 1) / 2), 'int16');
    
    if(y2_index >= 1)
        y2(n) = values(y2_index);
    elseif (y2_index <= 0)
        y2(n) = values(y2_index + N_samp);
    end
end

y = y2-y1;

subplot(4,1,2);
plot(y);
title('Vystup po pruchodu linearnim High-Pass filtrem');
ylabel('Napeti [mV]');
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Neline�rn� Low-Pass Filtr         %
%                                         %
%   (sign�l)^2 -> sumace(W)               %
%   W - d�lka okna                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

y_squared = y.^2;
W = LP_filter_window;                                   

y_low = zeros(1, size(values, 2));
for i = 1 : size(values, 2)
    sum = 0;
    for n = i : i + W
        if (i + W) <= size(values, 2)
            sum = sum + y_squared(n);
        else
            sum = sum + y_squared((i + W) - size(values, 2));
        end
    end
    y_low(i) = sum;
end

subplot(4,1,3);
plot(y_low);
title('Vystup po pruchodu nelinearnim Low-Pass filtrem');
ylabel('Napeti [mV]');
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                Pr�hov�n�                %
%                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
threshold = mean(y_low);                  % po?�te?n� treshhold
QRS = zeros(1, size(values, 2));          % inicializace pole;
Wt = Threshold_window;                    % d�lka okna hled�n� tresholdu

added = 0;
for i = 1 : size(values,2)
    if(mod(i,Wt)==0)
        added = 0;
    end
    
    if(i + Wt > size(values, 2))
        m = max(y_low(i : size(values,2)));
    else
        m = max(y_low(i : i + Wt));
    end
    
    % N�hodn� generov�n� alfy a gamy
    alpha = rand();
    gama = rand();
    if(gama < 0.5)
        gama = 0.15;
    else
        gama = 0.20;
    end
    
    % Pr�h podle Chena
    threshold = alpha * gama * m + (1-alpha)*threshold;
    
    if(y_low(i) > threshold)
        if(added == 0)
            delta = Delta_threshold;
            % Pokud byl nalezen peak - kouknout do okol� <-delta, delta> od
            % i a naj�t maximum. 
            % Slou�� ke korekci nalezen�ch peak? aby peak byl v nejvy���m
            % m�st?
            if((i-delta) >= 1 && (i+delta) <= size(values,2))
                [M,I] = max(y_low(i-delta:i+delta));
                index = i - delta + I;
                QRS(index) = 1;
                added = 1;
            end
        end
    end
end

result = subplot(4,1,4);
plot(values);
title('Detekovane R peaky QRS komplexu');
ylabel('Napeti [mV]');
hold on;

% Zobrazen� nalezen�ch Peak?
peaksCount = 0;
for i = 1 : size(values,2)
    if(QRS(i) == 1)
        plot(i, values(i), 'r*');
        hold on;
        peaksCount = peaksCount + 1;
    end
end

% V�po?et tepov� frekvence
BPM = peaksCount * time;
bpmControl = uicontrol('style','text', 'FontSize', 10);
set(bpmControl,'String',['Srdecni tepova frekvence: ', num2str(BPM), 'bpm'])
set(bpmControl,'Position',[0,30,300,20])
timeControl = uicontrol('style','text', 'FontSize', 10);
set(timeControl,'String',['Doba mereni signalu: ', num2str((size(values,2) / N_samp)), 's'])
set(timeControl,'Position',[0,10,300,20])